try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='log_analysis',
    version='0.1',
    description='Analyze Apache log files for file extensions with the biger impact',
    author='Sergios Lenis',
    author_email='sergioslenis@gmail.com',
    license='MIT',
    packages=['log_analysis'],
    include_package_data=True,
    install_requires=[ 'flask'],
        )

