import os
import log_analysis
import unittest
import tempfile
import json
from io import StringIO

class Log_analysisTestCase(unittest.TestCase):

    def setUp(self):
        # self.db_fd, log_analysis.app.config['DATABASE'] = tempfile.mkstemp()
        log_analysis.app.testing = True
        self.app = log_analysis.app.test_client()
        # test working page
    def test_api_hello(self):
        rtt= self.app.get('/')
        # print(rtt, rtt.data)
        assert b'Upload new File for Analysis' in rtt.data
        # test posting with no permiting
    def test_file_post_not_allowed(self):
        rtt=self.app.post('/', data=dict(
        title='<Hello>',
        text='<strong>HTML</strong> allowed here'
    ), follow_redirects=True)
        assert b'data.addRows( null);' in rtt.data
    

if __name__ == '__main__':
    unittest.main()