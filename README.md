## Requirments
### To install all the requirements run `pip install -e .` from the current directory

## Running
### To run the application 
* change directory into log_analysis and run `python log_analysis/log_analysis.py` 
* navigate to http://127.0.0.1:5000/ 
api endpoints 
* / loading of file and analysis

#### The application is tested to work in Ubuntu 16.04.

#### coverage and testing in the gitlab CI/CD 

#### to run tests manualy
after install `pip install -e . ` python tests/log_analysis_tests.py