from collections import Counter
import re, os

# all the helper functions
# 
ALLOWED_EXTENSIONS = set(['txt', 'log'])

# checking for the files to allow
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# parse log file and create dictionary of data
def analyse_log(file):
    # regular expresion for the .html?blabla part
    # that is not recognized by the splitext
    regex = re.compile(r"\.([a-z]{2,4})")
    # dictionary with extension as key and value
    # a list of values
    data_dict = {}
    # A counter to figure out the 20 most_frequent
    # extensions
    extensions_dict=Counter()
    # check the parse
    regected=[]
    sososo=0

    # enumerate log lines 
    for i,lline in enumerate(file):
        log_extension=None
        line=str(lline).rstrip().split(' ')
        
        if '.' in line[6]:
            log_url_path=os.path.splitext(line[6])
            log_extension=log_url_path[1]

            # failing extension separation check with regex
            # /blog/geekery/disabling-battery-in-ubuntu-vms.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+semicomplete%2Fmain+%28semicomplete.com+-+Jordan+Sissel%29
            # appers as .com+-+Jordan+Sissel%29 geting .html with regex
            if len(log_extension)>10 or log_extension=='':
                extension= regex.search(line[6])
                # print(extension)
                if extension:
                    # print('(((((((((((((((((((((((((((((')
                    log_extension=extension.group()
            # paths like /presentations/logstash-metrics-sf-2012.10/ that
            # return '' as extension removed
            if log_extension!='':
                # size is defined and not - allowed
                if line[9].isnumeric():
                    extensions_dict[log_extension]+=1

                    if log_extension not in data_dict:

                        data_dict[log_extension]={'data':[int(line[9])]}#,'codes':{line[8]},'request':{line[5]}}
                    else:
                        data_dict[log_extension]['data'].append(int(line[9]))
                        # data_dict[log_extension]['codes'].add(line[8])
                        # data_dict[log_extension]['request'].add(line[5])
                # check for rejected extensions
                else:
                    regected.append([log_extension,line])

        else:
            sososo+=1
    
    # print(sososo,len(regected),sum(extensions_dict.values()))

    # the 20 most frequent extensions and times they appear
    most_common_extensions=extensions_dict.most_common(20)
    return most_common_extensions,data_dict