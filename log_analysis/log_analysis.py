import os
from flask import Flask, render_template, url_for,make_response, request,flash, redirect
from flask import send_from_directory
from werkzeug.utils import secure_filename
import math
try:
    from log_analysis.utils import allowed_file, analyse_log
except:
    from utils import allowed_file, analyse_log
# global settings
DEBUG = False

# Allowing extensions txt and log for the uploads
ALLOWED_EXTENSIONS = set(['txt', 'log'])

# Max uploaded file size 
MAX_CONTENT_LENGTH = 16 * 1024 * 1024

# initializing 
app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('VIS_REPORT_LOG_SETTINGS', silent=True)



# default route and uploading of file
@app.route('/',methods=['GET','POST'])
def upload_analyze_log():
    stats_col=None
    stats=None
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']

        # If no file selected then an epty part without 
        # filename submited
        if file.filename == '':
            return redirect(request.url)
        # filename allowed 
        if file and allowed_file(file.filename):
            # get the most common and the data dict
            most_frequent_extensions,data_dict = analyse_log(file)
            stats=[[k,sum(data_dict[k]['data'])/v,sum(data_dict[k]['data'])] for k,v in most_frequent_extensions]
            stats.sort(key=lambda x: x[1],reverse=False)

            stats_col=[[k[0],k[1]] for k in stats]
            stats=[[k[0],math.log(k[1]),math.log(k[2])] for k in stats]
    return render_template('report.html', data= stats_col,data_tot=stats)
            


if __name__== '__main__':
    app.debug = True
    app.run()